package br.com.ozeano.curso.api.bb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoApiBbApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoApiBbApplication.class, args);
	}

}
